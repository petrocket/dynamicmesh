
#pragma once

//#include <ProjectDefines.h>
//#include <VertexFormats.h>

#include <AzCore/EBus/EBus.h>
#include <AzCore/Component/ComponentBus.h>

namespace DynamicMesh
{
    typedef AZ::Uuid DynamicMeshId;

    class DynamicMeshRequests
        : public AZ::ComponentBus
    {
    public:
        /**
        * Create a dynamic mesh.
        * @return the DynamicMeshId of the new dynamic mesh.
        */
        virtual DynamicMeshId CreateDynamicMesh() = 0;

        /**
        * Destroy a dynamic mesh.
        * @param id DynamicMeshId of the dynamic mesh to destroy.
        */
        virtual void DestroyDynamicMesh(const DynamicMeshId& id) = 0;

        /**
        * Update a dynamic mesh.  For now just support one vertex format.
        * @param id DynamicMeshId of the dynamic mesh to destroy.
        * @param vertices a vector of vertex information 
        * @param tangents a vector of tangents (optional) 
        */
        virtual void UpdateDynamicMesh(const DynamicMeshId& id, const AZStd::vector<SVF_P3F_C4B_T2F> vertices, const AZStd::vector<vtx_idx> indices, const AZStd::vector<SPipTangents> tangents) = 0;

        // @TODO update methods to only update the vertices and tangents or indices
    };
    using DynamicMeshRequestBus = AZ::EBus<DynamicMeshRequests>;
} // namespace flite
