
#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/Entity.h>
#include <AzCore/Math/Transform.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Math/Aabb.h>
#include <AzCore/Component/TickBus.h>

#include <AzFramework/Asset/SimpleAsset.h>
#include <LmbrCentral/Rendering/MaterialAsset.h>
#include <LmbrCentral/Rendering/MaterialOwnerBus.h>
#include <LmbrCentral/Rendering/RenderNodeBus.h>

#include "DynamicMeshComponentRenderNode.h"

#include <IEntityRenderState.h>
#include <AzCore/RTTI/TypeInfo.h>
#include <IMaterial.h>
#include <IRenderMesh.h>

namespace DynamicMesh 
{
    class DynamicMeshComponent
        : public AZ::Component
        , public LmbrCentral::MaterialOwnerRequestBus::Handler
        , public LmbrCentral::RenderNodeRequestBus::Handler
    {
    public:
        AZ_COMPONENT(DynamicMeshComponent, "{F150C3A8-A018-45EB-8F13-2D25C9ECDD86}");
        ~DynamicMeshComponent() override = default;

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
        {
            provided.push_back(AZ_CRC("DynamicMeshService"));
        }

        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
        {
            incompatible.push_back(AZ_CRC("DynamicMeshService"));
        }

        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required)
        {
            required.push_back(AZ_CRC("TransformService", 0x8ee22c50));
        }

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        bool ReadInConfig(const AZ::ComponentConfig* baseConfig) override;
        bool WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const override;
        ////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // MaterialOwnerRequestBus interface implementation
        bool IsMaterialOwnerReady() override;
        void SetMaterial(_smart_ptr<IMaterial> material) override;
        _smart_ptr<IMaterial> GetMaterial() override;
        void SetMaterialHandle(const LmbrCentral::MaterialHandle& materialHandle) override;
        LmbrCentral::MaterialHandle GetMaterialHandle() override;
        void SetMaterialParamVector4(const AZStd::string& name, const AZ::Vector4& value, int materialId = 1) override;
        void SetMaterialParamVector3(const AZStd::string& name, const AZ::Vector3& value, int materialId = 1) override;
        void SetMaterialParamColor(const AZStd::string& name, const AZ::Color& value, int materialId = 1) override;
        void SetMaterialParamFloat(const AZStd::string& name, float value, int materialId = 1) override;
        AZ::Vector4 GetMaterialParamVector4(const AZStd::string& name, int materialId = 1) override;
        AZ::Vector3 GetMaterialParamVector3(const AZStd::string& name, int materialId = 1) override;
        AZ::Color   GetMaterialParamColor(const AZStd::string& name, int materialId = 1) override;
        float       GetMaterialParamFloat(const AZStd::string& name, int materialId = 1) override;
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // RenderNodeRequestBus
        IRenderNode* GetRenderNode() override;
        float GetRenderNodeRequestBusOrder() const override;
        static const float s_renderNodeRequestBusOrder;
        //////////////////////////////////////////////////////////////////////////

    protected:
        // the render node stores the config for now so it can access the properties
        DynamicMeshComponentRenderNode m_renderNode;
    };
}