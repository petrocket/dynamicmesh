
#include "DynamicMesh_precompiled.h"

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/RTTI/BehaviorContext.h>
#include "DynamicMeshComponent.h"

#include <MathConversion.h>
#include "LCGRandom.h"
#include "PNoise3.h"

#include <I3DEngine.h>
#include "RendElement.h"

namespace DynamicMesh 
{
    const float DynamicMeshComponent::s_renderNodeRequestBusOrder = 100.f;

    void DynamicMeshComponent::Reflect(AZ::ReflectContext* context)
    {
        DynamicMeshComponentRenderNode::Reflect(context);

        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<DynamicMeshComponent, AZ::Component>()
                ->Version(1)
                ->Field("Render Node", &DynamicMeshComponent::m_renderNode);
        }
    }

    void DynamicMeshComponent::Init()
    {
    }

    void DynamicMeshComponent::Activate()
    {
        m_renderNode.AttachToEntity(m_entity->GetId());

        AZ::Transform tm;
        AZ::TransformBus::EventResult(tm, GetEntityId(), &AZ::TransformBus::Events::GetWorldTM);

        //TODO !
        //AZ::Aabb worldBoundingBox = AZ::Aabb::CreateCenterRadius(tm.GetPosition(), m_radius);
        //m_renderNode.SetBBox(AZAabbToLyAABB(worldBoundingBox));
        m_renderNode.RegisterWithRenderer(true);

        LmbrCentral::MaterialOwnerRequestBus::Handler::BusConnect(m_entity->GetId());
        LmbrCentral::RenderNodeRequestBus::Handler::BusConnect(m_entity->GetId());
        LmbrCentral::MaterialOwnerNotificationBus::Event(GetEntityId(), &LmbrCentral::MaterialOwnerNotificationBus::Events::OnMaterialOwnerReady);
    }

    void DynamicMeshComponent::Deactivate()
    {
        m_renderNode.RegisterWithRenderer(false);
        m_renderNode.AttachToEntity(AZ::EntityId());

        LmbrCentral::RenderNodeRequestBus::Handler::BusDisconnect();
        LmbrCentral::MaterialOwnerRequestBus::Handler::BusDisconnect();
    }

    bool DynamicMeshComponent::IsMaterialOwnerReady()
    {
        return true; //m_renderNode && m_renderNode->IsReady();
    }

    void DynamicMeshComponent::SetMaterial(_smart_ptr<IMaterial> material)
    {
        if (material && material->IsSubMaterial())
        {
            AZ_Error("MaterialOwnerRequestBus", false, "Material Owner cannot be given a Sub-Material.");
        }
        else
        {
            m_renderNode.SetMaterial(material);
        }
    }

    _smart_ptr<IMaterial> DynamicMeshComponent::GetMaterial()
    {
        _smart_ptr<IMaterial> material = m_renderNode.GetMaterial();

        if (!m_renderNode.IsReady())
        {
            if (material)
            {
                AZ_Warning("MaterialOwnerRequestBus", false, "A Material was found, but Material Owner is not ready. May have unexpected results. (Try using MaterialOwnerNotificationBus.OnMaterialOwnerReady or MaterialOwnerRequestBus.IsMaterialOwnerReady)");
            }
            else
            {
                AZ_Error("MaterialOwnerRequestBus", false, "Material Owner is not ready and no Material was found. Assets probably have not finished loading yet. (Try using MaterialOwnerNotificationBus.OnMaterialOwnerReady or MaterialOwnerRequestBus.IsMaterialOwnerReady)");
            }
        }

        return material;
    }

    void DynamicMeshComponent::SetMaterialHandle(const LmbrCentral::MaterialHandle& materialHandle)
    {
        SetMaterial(materialHandle.m_material);
    }

    LmbrCentral::MaterialHandle DynamicMeshComponent::GetMaterialHandle()
    {
        LmbrCentral::MaterialHandle m;
        m.m_material = GetMaterial();
        return m;
    }

    void DynamicMeshComponent::SetMaterialParamVector4(const AZStd::string& name, const AZ::Vector4& value, int materialId)
    {
        if (GetMaterial())
        {
            //if (!IsMaterialCloned())
            //{
            //    CloneMaterial();
            //}

            const int materialIndex = materialId - 1;
            Vec4 vec4(value.GetX(), value.GetY(), value.GetZ(), value.GetW());
            const bool success = GetMaterial()->SetGetMaterialParamVec4(name.c_str(), vec4, false, true, materialIndex);
            AZ_Error("Material Owner", success, "Failed to set Material ID %d, param '%s'.", materialId, name.c_str());
        }
    }

    void DynamicMeshComponent::SetMaterialParamVector3(const AZStd::string& name, const AZ::Vector3& value, int materialId)
    {
        if (GetMaterial())
        {
            //if (!IsMaterialCloned())
            //{
            //    CloneMaterial();
            //}

            const int materialIndex = materialId - 1;
            Vec3 vec3(value.GetX(), value.GetY(), value.GetZ());
            const bool success = GetMaterial()->SetGetMaterialParamVec3(name.c_str(), vec3, false, true, materialIndex);
            AZ_Error("Material Owner", success, "Failed to set Material ID %d, param '%s'.", materialId, name.c_str());
        }
    }

    void DynamicMeshComponent::SetMaterialParamColor(const AZStd::string& name, const AZ::Color& value, int materialId)
    {
        if (GetMaterial())
        {
            // When value had garbage data is was not only making the material render black, it also corrupted something
            // on the GPU, making black boxes flicker over the sky.
            // It was garbage due to a bug in the Color object node where all fields have to be set to some value manually; the default is not 0.
            if ((value.GetR() < 0 || value.GetR() > 1) ||
                (value.GetG() < 0 || value.GetG() > 1) ||
                (value.GetB() < 0 || value.GetB() > 1) ||
                (value.GetA() < 0 || value.GetA() > 1))
            {
                return;
            }

            //if (!IsMaterialCloned())
            //{
            //    CloneMaterial();
            //}

            const int materialIndex = materialId - 1;
            Vec4 vec4(value.GetR(), value.GetG(), value.GetB(), value.GetA());
            const bool success = GetMaterial()->SetGetMaterialParamVec4(name.c_str(), vec4, false, true, materialIndex);
            AZ_Error("Material Owner", success, "Failed to set Material ID %d, param '%s'.", materialId, name.c_str());
        }
    }

    void DynamicMeshComponent::SetMaterialParamFloat(const AZStd::string& name, float value, int materialId  /*= 0*/)
    {
        if (GetMaterial())
        {
            //if (!IsMaterialCloned())
            //{
            //    CloneMaterial();
            //}

            const int materialIndex = materialId - 1;
            const bool success = GetMaterial()->SetGetMaterialParamFloat(name.c_str(), value, false, true, materialIndex);
            AZ_Error("Material Owner", success, "Failed to set Material ID %d, param '%s'.", materialId, name.c_str());
        }
    }

    AZ::Vector4 DynamicMeshComponent::GetMaterialParamVector4(const AZStd::string& name, int materialId)
    {
        AZ::Vector4 value = AZ::Vector4::CreateZero();

        _smart_ptr<IMaterial> material = GetMaterial();
        if (material)
        {
            const int materialIndex = materialId - 1;
            Vec4 vec4;
            if (material->SetGetMaterialParamVec4(name.c_str(), vec4, true, true, materialIndex))
            {
                value.Set(vec4.x, vec4.y, vec4.z, vec4.w);
            }
            else
            {
                AZ_Error("Material Owner", false, "Failed to read Material ID %d, param '%s'.", materialId, name.c_str());
            }
        }

        return value;
    }

    AZ::Vector3 DynamicMeshComponent::GetMaterialParamVector3(const AZStd::string& name, int materialId)
    {
        AZ::Vector3 value = AZ::Vector3::CreateZero();

        _smart_ptr<IMaterial> material = GetMaterial();
        if (material)
        {
            const int materialIndex = materialId - 1;
            Vec3 vec3;
            if (material->SetGetMaterialParamVec3(name.c_str(), vec3, true, true, materialIndex))
            {
                value.Set(vec3.x, vec3.y, vec3.z);
            }
            else
            {
                AZ_Error("Material Owner", false, "Failed to read Material ID %d, param '%s'.", materialId, name.c_str());
            }
        }

        return value;
    }

    AZ::Color DynamicMeshComponent::GetMaterialParamColor(const AZStd::string& name, int materialId)
    {
        AZ::Color value = AZ::Color::CreateZero();

        _smart_ptr<IMaterial> material = GetMaterial();
        if (material)
        {
            const int materialIndex = materialId - 1;
            Vec4 vec4;
            if (material->SetGetMaterialParamVec4(name.c_str(), vec4, true, true, materialIndex))
            {
                value.Set(vec4.x, vec4.y, vec4.z, vec4.w);
            }
            else
            {
                AZ_Error("Material Owner", false, "Failed to read Material ID %d, param '%s'.", materialId, name.c_str());
            }
        }

        return value;
    }

    float DynamicMeshComponent::GetMaterialParamFloat(const AZStd::string& name, int materialId)
    {
        float value = 0.0f;

        _smart_ptr<IMaterial> material = GetMaterial();
        if (material)
        {
            const int materialIndex = materialId - 1;
            const bool success = material->SetGetMaterialParamFloat(name.c_str(), value, true, true, materialIndex);
            AZ_Error("Material Owner", success, "Failed to read Material ID %d, param '%s'.", materialId, name.c_str());
        }

        return value;
    }

    IRenderNode* DynamicMeshComponent::GetRenderNode()
    {
        return &m_renderNode;
    }

    float DynamicMeshComponent::GetRenderNodeRequestBusOrder() const
    {
        return s_renderNodeRequestBusOrder;
    }

    bool DynamicMeshComponent::ReadInConfig(const AZ::ComponentConfig* baseConfig)
    {
        if (auto config = azrtti_cast<const DynamicMeshComponentConfig*>(baseConfig))
        {
            m_renderNode.m_config = *config;
            return true;
        }
        return false;
    }

    bool DynamicMeshComponent::WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const
    {
        if (auto config = azrtti_cast<DynamicMeshComponentConfig*>(outBaseConfig))
        {
            *config = m_renderNode.m_config;
            return true;
        }
        return false;
    }
}
