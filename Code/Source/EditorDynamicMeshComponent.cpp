
#include "DynamicMesh_precompiled.h"

#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/RTTI/BehaviorContext.h>
#include "EditorDynamicMeshComponent.h"

#include <MathConversion.h>
#include "LCGRandom.h"
#include "PNoise3.h"

#include <I3DEngine.h>
#include "RendElement.h"

namespace DynamicMesh 
{
    AZ::u32 DynamicMeshComponentConfig::OnAssetPropertyChanged()
    {
        _smart_ptr<IMaterial> material = nullptr;
        const AZStd::string& materialPath = m_materialAsset.GetAssetPath();
        if (!materialPath.empty())
        {
            material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(materialPath.c_str());
            AZ_Warning("DynamicMeshComponent", material != gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial(),
                "Failed to load material \"%s\".",
                materialPath.c_str());
        }
        else
        {
            material = gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial();
        }

        if (m_renderNode)
        {
            m_renderNode->SetMaterial(material);
        }
        return AZ::Edit::PropertyRefreshLevels::None;
    }

    AZ::u32 DynamicMeshComponentConfig::OnMinorChanged()
    {
        if (m_renderNode)
        {
            // could expose ApplyRenderOptions() and friend the class or use a function callback on the rendernode
            m_renderNode->SetViewDistanceMultiplier(m_viewDistMultiplier);
            m_renderNode->m_fWSMaxViewDist = m_maxViewDist;
        }
        return AZ::Edit::PropertyRefreshLevels::None;
    }

    void EditorDynamicMeshComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<EditorDynamicMeshComponent, AZ::Component>()
                ->Version(1)
                ->Field("Render Node", &EditorDynamicMeshComponent::m_renderNode);

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<EditorDynamicMeshComponent>("DynamicMesh", "")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::Category, "Rendering")
                        ->Attribute(AZ::Edit::Attributes::PreferNoViewportIcon, true)
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ->DataElement(0, &EditorDynamicMeshComponent::m_renderNode, "Render Node", "Render Node")
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                        ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ;
                ec->Class<DynamicMeshComponentRenderNode>("DynamicMeshComponent Rendering", "Rendering Material for DynamicMesh")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                        ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game", 0x232b318c))
                    ->DataElement(0, &DynamicMeshComponentRenderNode::m_config, "Config", "Config")
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                        ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                    ;
                ec->Class<DynamicMeshComponentConfig>("DynamicMeshComponent Config", "Config for DynamicMesh")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                        ->Attribute(AZ::Edit::Attributes::Visibility, AZ_CRC("PropertyVisibility_ShowChildrenOnly", 0xef428f20))
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game", 0x232b318c))

                    ->DataElement(AZ::Edit::UIHandlers::Default, &DynamicMeshComponentConfig::m_materialAsset, "Material", "Material to use.")
                        ->Attribute(AZ::Edit::Attributes::ChangeNotify, &DynamicMeshComponentConfig::OnAssetPropertyChanged)

                    ->ClassElement(AZ::Edit::ClassElements::Group, "Options")
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, false)

                    ->DataElement(AZ::Edit::UIHandlers::Slider, &DynamicMeshComponentConfig::m_opacity, "Opacity", "Opacity value")
                        ->Attribute(AZ::Edit::Attributes::Min, 0.f)
                        ->Attribute(AZ::Edit::Attributes::Max, 1.f)
                        ->Attribute(AZ::Edit::Attributes::Step, 0.1f)
                        ->Attribute(AZ::Edit::Attributes::ChangeNotify, &DynamicMeshComponentConfig::OnMinorChanged)
                    ->DataElement(AZ::Edit::UIHandlers::Default, &DynamicMeshComponentConfig::m_maxViewDist, "Max view distance", "Maximum view distance in meters.")
                        ->Attribute(AZ::Edit::Attributes::Suffix, " m")
                        ->Attribute(AZ::Edit::Attributes::Min, 0.f)
                        ->Attribute(AZ::Edit::Attributes::Max, 1024.f)
                        ->Attribute(AZ::Edit::Attributes::Step, 0.1f)
                        ->Attribute(AZ::Edit::Attributes::ChangeNotify, &DynamicMeshComponentConfig::OnMinorChanged)
                    ->DataElement(AZ::Edit::UIHandlers::Default, &DynamicMeshComponentConfig::m_viewDistMultiplier, "View distance multiplier", "Adjusts max view distance. If 1.0 then default is used. 1.1 would be 10% further than default.")
                        ->Attribute(AZ::Edit::Attributes::Suffix, "x")
                        ->Attribute(AZ::Edit::Attributes::Min, 0.f)
                    ->Attribute(AZ::Edit::Attributes::ChangeNotify, &DynamicMeshComponentConfig::OnMinorChanged)
                    ->DataElement(AZ::Edit::UIHandlers::Default, &DynamicMeshComponentConfig::m_castShadows, "Cast shadows", "Casts shadows.")
                        ->Attribute(AZ::Edit::Attributes::ChangeNotify, &DynamicMeshComponentConfig::OnMinorChanged)
                    ;
            }
        }
    }

    void EditorDynamicMeshComponent::Init()
    {
    }

    void EditorDynamicMeshComponent::Activate()
    {
        m_renderNode.AttachToEntity(m_entity->GetId());

        AZ::Transform tm;
        AZ::TransformBus::EventResult(tm, GetEntityId(), &AZ::TransformBus::Events::GetWorldTM);

        //TODO !
        //AZ::Aabb worldBoundingBox = AZ::Aabb::CreateCenterRadius(tm.GetPosition(), m_radius);
        //m_renderNode.SetBBox(AZAabbToLyAABB(worldBoundingBox));
        m_renderNode.RegisterWithRenderer(true);

        LmbrCentral::MaterialOwnerRequestBus::Handler::BusConnect(m_entity->GetId());
        LmbrCentral::RenderNodeRequestBus::Handler::BusConnect(m_entity->GetId());
        AzToolsFramework::EditorVisibilityNotificationBus::Handler::BusConnect(GetEntityId());
        AzFramework::EntityDebugDisplayEventBus::Handler::BusConnect(GetEntityId());
    }

    void EditorDynamicMeshComponent::Deactivate()
    {
        m_renderNode.RegisterWithRenderer(false);
        m_renderNode.AttachToEntity(AZ::EntityId());

        LmbrCentral::MaterialOwnerRequestBus::Handler::BusDisconnect();
        LmbrCentral::RenderNodeRequestBus::Handler::BusDisconnect();
        AzToolsFramework::EditorVisibilityNotificationBus::Handler::BusDisconnect();
        AzFramework::EntityDebugDisplayEventBus::Handler::BusDisconnect();
    }

    void EditorDynamicMeshComponent::SetMaterial(_smart_ptr<IMaterial> material)
    {
        if (material && material->IsSubMaterial())
        {
            AZ_Error("MaterialOwnerRequestBus", false, "Material Owner cannot be given a Sub-Material.");
        }
        else
        {
            m_renderNode.SetMaterial(material);
        }
    }

    _smart_ptr<IMaterial> EditorDynamicMeshComponent::GetMaterial()
    {
        _smart_ptr<IMaterial> material = m_renderNode.GetMaterial();

        if (!m_renderNode.IsReady())
        {
            if (material)
            {
                AZ_Warning("MaterialOwnerRequestBus", false, "A Material was found, but Material Owner is not ready. May have unexpected results. (Try using MaterialOwnerNotificationBus.OnMaterialOwnerReady or MaterialOwnerRequestBus.IsMaterialOwnerReady)");
            }
            else
            {
                AZ_Error("MaterialOwnerRequestBus", false, "Material Owner is not ready and no Material was found. Assets probably have not finished loading yet. (Try using MaterialOwnerNotificationBus.OnMaterialOwnerReady or MaterialOwnerRequestBus.IsMaterialOwnerReady)");
            }
        }

        return material;
    }

    bool EditorDynamicMeshComponent::ReadInConfig(const AZ::ComponentConfig* baseConfig)
    {
        if (auto config = azrtti_cast<const DynamicMeshComponentConfig*>(baseConfig))
        {
            static_cast<DynamicMeshComponentConfig&>(m_renderNode.m_config) = *config;
            return true;
        }
        return false;
    }

    bool EditorDynamicMeshComponent::WriteOutConfig(AZ::ComponentConfig* outBaseConfig) const
    {
        if (auto config = azrtti_cast<DynamicMeshComponentConfig*>(outBaseConfig))
        {
            *config = m_renderNode.m_config;
            return true;
        }
        return false;
    }

    void EditorDynamicMeshComponent::BuildGameEntity(AZ::Entity* gameEntity)
    {
        gameEntity->CreateComponent<DynamicMeshComponent>()->SetConfiguration(m_renderNode.m_config);
    }

    void EditorDynamicMeshComponent::OnAssetPropertyChanged()
    {
        _smart_ptr<IMaterial> material = nullptr;
        const AZStd::string& materialPath = m_renderNode.m_config.m_materialAsset.GetAssetPath();
        if (!materialPath.empty())
        {
            material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(materialPath.c_str());
            AZ_Warning("DynamicMeshComponent", material != gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial(),
                "Failed to load material \"%s\".",
                materialPath.c_str());
        }
        else
        {
            material = gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial();
        }

        SetMaterial(material);
    }

    IRenderNode* EditorDynamicMeshComponent::GetRenderNode()
    {
        return &m_renderNode;
    }

    float EditorDynamicMeshComponent::GetRenderNodeRequestBusOrder() const
    {
        return s_renderNodeRequestBusOrder;
    }

    void EditorDynamicMeshComponent::DisplayEntityViewport(const AzFramework::ViewportInfo& viewportInfo, AzFramework::DebugDisplayRequests& debugDisplay)
    {
        // Never draw the default sphere 
        //handled = true;
    }

    void EditorDynamicMeshComponent::OnEntityVisibilityChanged(bool visibility)
    {
        m_renderNode.Hide(!visibility);
    }
}
