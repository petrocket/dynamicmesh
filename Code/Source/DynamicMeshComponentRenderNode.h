
#pragma once

#include <AzCore/Component/Component.h>
#include <AzCore/Component/Entity.h>
#include <AzCore/Component/TransformBus.h>
#include <AzCore/Component/TickBus.h>
#include <AzCore/Math/Transform.h>
#include <AzCore/Math/Aabb.h>

#include <LmbrCentral/Rendering/MaterialAsset.h>

#include <IEntityRenderState.h>
#include <DynamicMesh/DynamicMeshBus.h>

#include <IMaterial.h>
#include <IRenderMesh.h>

class CRendElementBase;

namespace DynamicMesh 
{
    /**
    * Configuration data for the DynamicMeshComponent.
    */
    class DynamicMeshComponentConfig
        : public AZ::ComponentConfig
    {
    public:
        AZ_CLASS_ALLOCATOR(DynamicMeshComponentConfig, AZ::SystemAllocator, 0);
        AZ_RTTI(DynamicMeshComponentConfig, "{3EA4CD8F-9F7B-489F-A9BC-80DCBF42A830}", ComponentConfig);
        static void Reflect(AZ::ReflectContext* context);

        AzFramework::SimpleAssetReference<LmbrCentral::MaterialAsset> m_materialAsset;
        float m_opacity = 1.f;
        bool m_castShadows = true;
        float m_maxViewDist = 1024.f;
        float m_viewDistMultiplier = 1.f;

        AZ::u32 OnAssetPropertyChanged();
        AZ::u32 OnMinorChanged();

        IRenderNode* m_renderNode = nullptr;
    };

    class DynamicMeshComponentRenderNode
        : public IRenderNode
        , public AZ::TransformNotificationBus::Handler
        , protected DynamicMeshRequestBus::Handler
    {
        friend class DynamicMeshComponent;
    public:
        using MaterialPtr = _smart_ptr < IMaterial >;
        using RenderMeshPtr = _smart_ptr < IRenderMesh >;

        AZ_TYPE_INFO(DynamicMeshComponentRenderNode, "{6D5D0259-C384-4317-B1CB-12FE3E93507F}");

        DynamicMeshComponentRenderNode();
        ~DynamicMeshComponentRenderNode() override;

        static void Reflect(AZ::ReflectContext* context);

        //! Notifies render node which entity owns it, for subscribing to transform
        //! bus, etc.
        void AttachToEntity(AZ::EntityId id);

        //! Updates the render node's world transform based on the entity's.
        void UpdateWorldTransform(const AZ::Transform& entityTransform);

        //! Computes world-space AABB.
        AZ::Aabb CalculateWorldAABB() const;

        //! Computes local-space AABB.
        AZ::Aabb CalculateLocalAABB() const;

        //////////////////////////////////////////////////////////////////////////
        // AZ::TransformNotificationBus::Handler interface implementation
        void OnTransformChanged(const AZ::Transform& local, const AZ::Transform& world) override;
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // DynamicMeshBus::Handler interface implementation
        DynamicMeshId CreateDynamicMesh() override;
        void DestroyDynamicMesh(const DynamicMeshId& id) override;
        void UpdateDynamicMesh(const DynamicMeshId& id, const AZStd::vector<SVF_P3F_C4B_T2F> vertices, const AZStd::vector<vtx_idx> indices, const AZStd::vector<SPipTangents> tangents) override;
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        // IRenderNode interface implementation
        void Render(const struct SRendParams& inRenderParams, const struct SRenderingPassInfo& passInfo) override;
        EERType GetRenderNodeType() override;
        const char* GetName() const override;
        const char* GetEntityClassName() const override;
        Vec3 GetPos(bool bWorldOnly = true) const override;
        const AABB GetBBox() const override;
        void SetBBox(const AABB& WSBBox) override;
        void OffsetPosition(const Vec3& delta) override;
        struct IPhysicalEntity* GetPhysics() const override;
        void SetPhysics(IPhysicalEntity* pPhys) override;
        void SetMaterial(_smart_ptr<IMaterial> pMat) override;
        _smart_ptr<IMaterial> GetMaterial(Vec3* pHitPos = nullptr) override;
        _smart_ptr<IMaterial> GetMaterialOverride() override;
        IStatObj* GetEntityStatObj(unsigned int nPartId = 0, unsigned int nSubPartId = 0, Matrix34A* pMatrix = nullptr, bool bReturnOnlyVisible = false) override;
        _smart_ptr<IMaterial> GetEntitySlotMaterial(unsigned int nPartId, bool bReturnOnlyVisible = false, bool* pbDrawNear = nullptr) override;
        ICharacterInstance* GetEntityCharacter(unsigned int nSlot = 0, Matrix34A* pMatrix = nullptr, bool bReturnOnlyVisible = false) override;
        float GetMaxViewDist() override;
        void GetMemoryUsage(class ICrySizer* pSizer) const override;
        AZ::EntityId GetEntityId() override { return m_attachedToEntityId; }
        float GetUniformScale() override;
        float GetColumnScale(int column) override;
        //////////////////////////////////////////////////////////////////////////

        //! Registers or unregisters our render node with the render.
        void RegisterWithRenderer(bool registerWithRenderer);
        bool IsRegisteredWithRenderer() const { return m_isRegisteredWithRenderer; }

        DynamicMeshComponentConfig m_config;

    protected:
        //! Applies configured render options to the render node.
        void ApplyRenderOptions();

        //! Computes the entity-relative (local space) bounding box for
        //! the mesh.
        virtual void UpdateLocalBoundingBox();

        //! Updates the world-space bounding box and world space transform
        //! for the mesh.
        void UpdateWorldBoundingBox();

        AZ::EntityId m_attachedToEntityId;

        //! Currently-assigned material
        MaterialPtr m_material;

        //! World and render transforms.
        //! These are equivalent, but for different math libraries.
        AZ::Transform m_worldTransform;
        Matrix34 m_renderTransform;

        //! Local and world bounding boxes.
        AABB m_localBoundingBox;
        AABB m_worldBoundingBox;

        //! Identifies whether we've already registered our node with the renderer.
        bool m_isRegisteredWithRenderer;

        float m_maxViewDist;

        bool m_objectMoved;

        CRendElementBase* m_renderElement;

        AZStd::unordered_map<DynamicMeshId, RenderMeshPtr> m_dynamicMeshes;
    };
}