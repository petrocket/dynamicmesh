
#include "DynamicMesh_precompiled.h"
#include <platform_impl.h>

#include <AzCore/Memory/SystemAllocator.h>

#include "DynamicMeshComponent.h"

#if defined(DYNAMICMESH_EDITOR)
#include "EditorDynamicMeshComponent.h"
#endif

#include <IGem.h>

namespace DynamicMesh
{
    class DynamicMeshModule
        : public CryHooksModule
    {
    public:
        AZ_RTTI(DynamicMeshModule, "{1A695BF8-A06C-434E-A58D-78EF8EB0ACAC}", CryHooksModule);
        AZ_CLASS_ALLOCATOR(DynamicMeshModule, AZ::SystemAllocator, 0);

        DynamicMeshModule()
            : CryHooksModule()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                DynamicMeshComponent::CreateDescriptor(),
#if defined(DYNAMICMESH_EDITOR)
                EditorDynamicMeshComponent::CreateDescriptor(),
#endif
            });
        }
    };
}

// DO NOT MODIFY THIS LINE UNLESS YOU RENAME THE GEM
// The first parameter should be GemName_GemIdLower
// The second should be the fully qualified name of the class above
AZ_DECLARE_MODULE_CLASS(DynamicMesh_fc3a18af6b80417993fe862b0ebc8faa, DynamicMesh::DynamicMeshModule)
