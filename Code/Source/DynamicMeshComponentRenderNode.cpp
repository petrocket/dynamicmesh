
#include "DynamicMesh_precompiled.h"

#include <AzCore/Serialization/SerializeContext.h>
#include "DynamicMeshComponentRenderNode.h"

#include <MathConversion.h>
#include "LCGRandom.h"
#include "PNoise3.h"

#include <I3DEngine.h>
#include "RendElement.h"
#include "IRenderAuxGeom.h"

namespace DynamicMesh
{
    void DynamicMeshComponentConfig::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<DynamicMeshComponentConfig, AZ::ComponentConfig>()
                ->Version(1)
                ->Field("Material", &DynamicMeshComponentConfig::m_materialAsset)
                ->Field("Opacity", &DynamicMeshComponentConfig::m_opacity)
                ->Field("CastShadows", &DynamicMeshComponentConfig::m_castShadows)
                ->Field("MaxViewDistance", &DynamicMeshComponentConfig::m_maxViewDist)
                ->Field("ViewDistanceMultiplier", &DynamicMeshComponentConfig::m_viewDistMultiplier)
                ;
        }
    }

    DynamicMeshComponentRenderNode::DynamicMeshComponentRenderNode()
        : m_material(nullptr)
        , m_isRegisteredWithRenderer(false)
    {
        m_localBoundingBox.Reset();
        m_worldBoundingBox.Reset();
        m_worldTransform = AZ::Transform::CreateIdentity();
        m_renderTransform = Matrix34::CreateIdentity();
        m_config.m_renderNode = this;
    }

    DynamicMeshComponentRenderNode::~DynamicMeshComponentRenderNode()
    {
    }

    void DynamicMeshComponentRenderNode::Reflect(AZ::ReflectContext* context)
    {
        DynamicMeshComponentConfig::Reflect(context);

        AZ::SerializeContext* serializeContext = azrtti_cast<AZ::SerializeContext*>(context);
        if (serializeContext)
        {
            serializeContext->Class<DynamicMeshComponentRenderNode>()
                ->Version(1)
                ->Field("Config", &DynamicMeshComponentRenderNode::m_config);
            ;
        }
    }

    DynamicMeshId DynamicMeshComponentRenderNode::CreateDynamicMesh()
    {
        DynamicMeshId id = DynamicMeshId::Create();

        AZStd::string name = AZStd::string("DynamicMesh").append(id.ToString<AZStd::string>());
        ERenderMeshType bufferType = eRMT_Static; // @TODO support dynamic type too
        IRenderMesh::SInitParamerers params;
        params.pVertBuffer = nullptr;
        params.nVertexCount = 0;
        params.vertexFormat = eVF_P3F_C4B_T2F;
        params.pIndices = nullptr;
        params.nIndexCount = 0;
        params.nPrimetiveType = prtTriangleList;
        params.eType = bufferType;
        params.nRenderChunkCount = 0; // might be slighlty more efficient to set this to 1?
        params.bOnlyVideoBuffer = false;
        params.bPrecache = false;

        RenderMeshPtr mesh = gEnv->pRenderer->CreateRenderMesh("DynamicMeshComponentRenderNode", name.c_str(), &params, bufferType);
        if (mesh)
        {
            m_dynamicMeshes.insert({ id, mesh });
            return id;
        }

        return DynamicMeshId::CreateNull();
    }

    void DynamicMeshComponentRenderNode::DestroyDynamicMesh(const DynamicMeshId& id)
    {
        auto it = m_dynamicMeshes.find(id);
        if (it != m_dynamicMeshes.end())
        {
            m_dynamicMeshes.erase(it);
        }
    }

    void DynamicMeshComponentRenderNode::UpdateDynamicMesh(const DynamicMeshId& id, const AZStd::vector<SVF_P3F_C4B_T2F> vertices, const AZStd::vector<vtx_idx> indices, const AZStd::vector<SPipTangents> tangents)
    {
        auto it = m_dynamicMeshes.find(id);
        if (it != m_dynamicMeshes.end() && it->second)
        {
            RenderMeshPtr mesh = it->second;
            // we will manually lock/unlock
            bool requiresLock = false;
            mesh->LockForThreadAccess();

            // recompute the bounding box (TODO pass in to improve speed)
            AZ::Aabb aabb = AZ::Aabb::CreateNull();
            for (SVF_P3F_C4B_T2F vertex : vertices)
            {
                aabb.AddPoint(AZ::Vector3(vertex.xyz.x, vertex.xyz.y, vertex.xyz.z));
            }
            aabb.Translate(m_worldTransform.GetPosition());

            // vertices
            mesh->UpdateVertices(&vertices[0], vertices.size(), 0, VSF_GENERAL, 0u, requiresLock);
            // tangents
            mesh->UpdateVertices(&tangents[0], tangents.size(), 0, VSF_TANGENTS, 0u, requiresLock);
            // indices 
            mesh->UpdateIndices(&indices[0], indices.size(), 0, 0u, requiresLock);

            // note this needs to match what is used in the rendermesh creation above
            const AZ::Vertex::Format vertexFormat = eVF_P3F_C4B_T2F;
            mesh->SetChunk(m_material, 0, vertices.size(), 0, indices.size(), 1.f, vertexFormat, 0);

            mesh->UnLockForThreadAccess();

            // set the bounding box for the mesh
            mesh->SetBBox(AZVec3ToLYVec3(aabb.GetMin()), AZVec3ToLYVec3(aabb.GetMax()));

            // update the overall bounding box used for the visibility 
            // of the whole render node
            aabb = AZ::Aabb::CreateNull();
            Vec3 min, max;
            for (auto dynamicMesh : m_dynamicMeshes)
            {
                dynamicMesh.second->GetBBox(min, max);
                aabb.AddPoint(LYVec3ToAZVec3(min));
                aabb.AddPoint(LYVec3ToAZVec3(max));
            }

            // SetBBox updates the local and world bounding boxes for all the dynamic meshes
            SetBBox(AZAabbToLyAABB(aabb));

            // re-register so this rendernode is culled correctly
            if (m_isRegisteredWithRenderer)
            {
                gEnv->p3DEngine->RegisterEntity(this);
            }
        }
    }

    void DynamicMeshComponentRenderNode::AttachToEntity(AZ::EntityId id)
    {
        if (AZ::TransformNotificationBus::Handler::BusIsConnectedId(m_attachedToEntityId))
        {
            AZ::TransformNotificationBus::Handler::BusDisconnect(m_attachedToEntityId);
        }

        if (DynamicMeshRequestBus::Handler::BusIsConnectedId(m_attachedToEntityId))
        {
            DynamicMeshRequestBus::Handler::BusDisconnect(m_attachedToEntityId);
        }

        const AZStd::string& materialPath = m_config.m_materialAsset.GetAssetPath();

        if (!materialPath.empty())
        {
            m_material = gEnv->p3DEngine->GetMaterialManager()->LoadMaterial(materialPath.c_str());

            AZ_Warning("DynamicMeshComponent", m_material != gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial(),
                "Failed to load override material \"%s\".",
                materialPath.c_str());
        }
        else
        {
            m_material = gEnv->p3DEngine->GetMaterialManager()->GetDefaultMaterial();
        }

        if (id.IsValid())
        {
            if (!AZ::TransformNotificationBus::Handler::BusIsConnectedId(id))
            {
                AZ::TransformNotificationBus::Handler::BusConnect(id);
            }

            if (!DynamicMeshRequestBus::Handler::BusIsConnectedId(id))
            {
                DynamicMeshRequestBus::Handler::BusConnect(id);
            }

            AZ::Transform entityTransform = AZ::Transform::CreateIdentity();
            EBUS_EVENT_ID_RESULT(entityTransform, id, AZ::TransformBus, GetWorldTM);
            UpdateWorldTransform(entityTransform);
        }

        m_attachedToEntityId = id;
    }

    namespace DynamicMeshInternal
    {
        void UpdateRenderFlag(bool enable, int mask, unsigned int& flags)
        {
            if (enable)
            {
                flags |= mask;
            }
            else
            {
                flags &= ~mask;
            }
        }
    }


    //! Applies configured render options to the render node.
    void DynamicMeshComponentRenderNode::ApplyRenderOptions()
    {
        using DynamicMeshInternal::UpdateRenderFlag;
        unsigned int flags = GetRndFlags();

        UpdateRenderFlag(m_config.m_castShadows, ERF_CASTSHADOWMAPS | ERF_HAS_CASTSHADOWMAPS, flags);

        m_fWSMaxViewDist = m_config.m_maxViewDist;

        SetViewDistanceMultiplier(m_config.m_viewDistMultiplier);

        SetRndFlags(flags);
    }


    void DynamicMeshComponentRenderNode::Render(const struct SRendParams& inRenderParams, const struct SRenderingPassInfo& passInfo)
    {
        IRenderer* pRenderer(gEnv->pRenderer);
        if (!pRenderer || m_dynamicMeshes.empty())
        {
            return;
        }

        SRendParams rParams(inRenderParams);
        // Assign a unique pInstance pointer, otherwise effects involving SRenderObjData will not work for this object.  CEntityObject::Render does this for legacy entities.
        rParams.pInstance = this;
        rParams.pMaterial = m_material;
        rParams.pMatrix = &m_renderTransform;
        rParams.fAlpha = m_config.m_opacity;

        if (m_objectMoved)
        {
            rParams.dwFObjFlags |= FOB_DYNAMIC_OBJECT;
            m_objectMoved = false;
        }

        // get render objects
        CRenderObject* pRO = pRenderer->EF_GetObject_Temp(passInfo.ThreadID());
        if (!pRO)
        {
            return;
        }

        pRO->m_pRenderNode = this;
        pRO->m_II.m_Matrix = AZTransformToLYTransform(m_worldTransform);
        pRO->m_fSort = 0;
        pRO->m_fDistance = rParams.fDistance;
        pRO->m_fAlpha = rParams.fAlpha;

        for (auto it : m_dynamicMeshes)
        {
            RenderMeshPtr mesh = it.second;
            if (mesh)
            {
                mesh->Render(rParams, pRO, m_material, passInfo);
            }
        }

#ifndef RELEASE
        static ICVar* debugDraw = gEnv->pConsole->GetCVar("e_debugdraw");
        if (debugDraw && debugDraw->GetIVal() == -1)
        {
            pRenderer->GetIRenderAuxGeom()->DrawAABB(m_worldBoundingBox, false, ColorB(255, 0, 0), EBoundingBoxDrawStyle::eBBD_Faceted);

            // draw a label where the mesh SHOULD be
            //Vec3 pos = AZVec3ToLYVec3(m_worldTransform.GetTranslation());
            //float color[4] = { 0, 1, 1, 1 };
            //pRenderer->DrawLabelEx(pos, 1.3f, color, true, true, "DynamicMeshComp");
        }
#endif
    }

    //! Updates the render node's world transform based on the entity's.
    void DynamicMeshComponentRenderNode::UpdateWorldTransform(const AZ::Transform& entityTransform)
    {
        m_worldTransform = entityTransform;

        m_renderTransform = AZTransformToLYTransform(m_worldTransform);

        UpdateWorldBoundingBox();

        m_objectMoved = true;
    }

    //! Computes world-space AABB.
    AZ::Aabb DynamicMeshComponentRenderNode::CalculateWorldAABB() const
    {
        AZ::Aabb aabb = AZ::Aabb::CreateNull();
        if (!m_worldBoundingBox.IsReset())
        {
            aabb.AddPoint(LYVec3ToAZVec3(m_worldBoundingBox.min));
            aabb.AddPoint(LYVec3ToAZVec3(m_worldBoundingBox.max));
        }
        return aabb;
    }

    //! Computes local-space AABB.
    AZ::Aabb DynamicMeshComponentRenderNode::CalculateLocalAABB() const
    {
        AZ::Aabb aabb = AZ::Aabb::CreateNull();
        if (!m_localBoundingBox.IsReset())
        {
            aabb.AddPoint(LYVec3ToAZVec3(m_localBoundingBox.min));
            aabb.AddPoint(LYVec3ToAZVec3(m_localBoundingBox.max));
        }
        return aabb;
    }

    //! Computes the entity-relative (local space) bounding box for
    //! the assigned mesh.
    void DynamicMeshComponentRenderNode::UpdateLocalBoundingBox()
    {
        m_localBoundingBox.Reset();

        // TODO update bounds here

        UpdateWorldBoundingBox();
    }

    //! Updates the world-space bounding box and world space transform
    //! for the assigned mesh.
    void DynamicMeshComponentRenderNode::UpdateWorldBoundingBox()
    {
        m_worldBoundingBox.SetTransformedAABB(m_renderTransform, m_localBoundingBox);

        if (m_isRegisteredWithRenderer)
        {
            // Re-register with the renderer to update culling info
            gEnv->p3DEngine->RegisterEntity(this);
        }
    }

    // AZ::TransformNotificationBus::Handler interface implementation
    void DynamicMeshComponentRenderNode::OnTransformChanged(const AZ::Transform& local, const AZ::Transform& world)
    {
        // The entity to which we're attached has moved.
        UpdateWorldTransform(world);
    }

    EERType DynamicMeshComponentRenderNode::GetRenderNodeType()
    {
        return eERType_StaticMeshRenderComponent;
    }

    const char* DynamicMeshComponentRenderNode::GetName() const
    {
        return "DynamicMeshComponentRenderNode";
    }

    const char* DynamicMeshComponentRenderNode::GetEntityClassName()  const
    {
        return "DynamicMeshComponentRenderNode";
    }

    Vec3 DynamicMeshComponentRenderNode::GetPos(bool bWorldOnly) const
    {
        return m_renderTransform.GetTranslation();
    }

    const AABB DynamicMeshComponentRenderNode::GetBBox() const
    {
        return m_worldBoundingBox;
    }

    void DynamicMeshComponentRenderNode::SetBBox(const AABB& WSBBox)
    {
        m_worldBoundingBox = WSBBox;
        GetLocalBounds(m_localBoundingBox);
    }

    void DynamicMeshComponentRenderNode::OffsetPosition(const Vec3& delta)
    {
        // Recalculate local transform
        AZ::Transform localTransform = AZ::Transform::CreateIdentity();
        EBUS_EVENT_ID_RESULT(localTransform, m_attachedToEntityId, AZ::TransformBus, GetLocalTM);

        localTransform.SetTranslation(localTransform.GetTranslation() + LYVec3ToAZVec3(delta));
        EBUS_EVENT_ID(m_attachedToEntityId, AZ::TransformBus, SetLocalTM, localTransform);

        m_objectMoved = true;
    }

    struct IPhysicalEntity* DynamicMeshComponentRenderNode::GetPhysics() const
    {
        return nullptr;
    }

    void DynamicMeshComponentRenderNode::SetPhysics(IPhysicalEntity* pPhys)
    {
    }

    void DynamicMeshComponentRenderNode::SetMaterial(_smart_ptr<IMaterial> pMat)
    {
        m_material = pMat;
    }

    _smart_ptr<IMaterial> DynamicMeshComponentRenderNode::GetMaterial(Vec3* pHitPos)
    {
        return m_material;
    }

    _smart_ptr<IMaterial> DynamicMeshComponentRenderNode::GetMaterialOverride()
    {
        return nullptr;
    }

    IStatObj* DynamicMeshComponentRenderNode::GetEntityStatObj(unsigned int nPartId, unsigned int nSubPartId, Matrix34A* pMatrix, bool bReturnOnlyVisible)
    {
        return nullptr;
    }

    _smart_ptr<IMaterial> DynamicMeshComponentRenderNode::GetEntitySlotMaterial(unsigned int nPartId, bool bReturnOnlyVisible, bool* pbDrawNear)
    {
        if (0 == nPartId)
        {
            return m_material;
        }

        return nullptr;
    }

    ICharacterInstance* DynamicMeshComponentRenderNode::GetEntityCharacter(unsigned int nSlot, Matrix34A* pMatrix, bool bReturnOnlyVisible)
    {
        return nullptr;
    }

    float DynamicMeshComponentRenderNode::GetMaxViewDist()
    {
        return(m_config.m_maxViewDist * 0.75f * GetViewDistanceMultiplier());
    }

    void DynamicMeshComponentRenderNode::GetMemoryUsage(class ICrySizer* pSizer) const
    {
        pSizer->AddObjectSize(this);
    }

    float DynamicMeshComponentRenderNode::GetUniformScale()
    {
        AZ::Vector3 scales = m_worldTransform.RetrieveScale();
        AZ_Warning("DynamicMeshComponentRenderNode", (scales.GetX() == scales.GetY()) && (scales.GetY() == scales.GetZ()), "DynamicMeshComponentRenderNode scales are not uniform");
        return scales.GetX();
    }

    float DynamicMeshComponentRenderNode::GetColumnScale(int column)
    {
        AZ::Vector3 scales = m_worldTransform.RetrieveScale();
        switch (column)
        {
        case 0:
            return scales.GetX();
            break;
        case 1:
            return scales.GetY();
            break;
        case 2:
            return scales.GetZ();
            break;
        default:
            AZ_Warning("DynamicMeshComponentRenderNode", false, "DynamicMeshComponentRenderNode Column out of range");
            return 1.0f;
            break;
        }
        return 1.0f;
    }

    void DynamicMeshComponentRenderNode::RegisterWithRenderer(bool registerWithRenderer)
    {
        if (gEnv && gEnv->p3DEngine)
        {
            if (registerWithRenderer)
            {
                if (!m_isRegisteredWithRenderer)
                {
                    ApplyRenderOptions();

                    gEnv->p3DEngine->RegisterEntity(this);

                    m_isRegisteredWithRenderer = true;
                }
            }
            else
            {
                if (m_isRegisteredWithRenderer)
                {
                    gEnv->p3DEngine->FreeRenderNodeState(this);
                    m_isRegisteredWithRenderer = false;
                }
            }
        }
    }

}